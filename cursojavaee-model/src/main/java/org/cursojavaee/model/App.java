package org.cursojavaee.model;

import org.apache.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	private final static Logger logger = Logger.getLogger(App.class);
			
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
	
    
	
}
